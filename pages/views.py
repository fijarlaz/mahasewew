from django.shortcuts import render
from .models import *
from django.core.paginator import Paginator
# Create your views here.
def index(request):
	response = {}
	response['posts'] = Room.objects.all()
	response['apart'] = Apartment.objects.all()
	response['testimonial'] = Testimonial.objects.all()
	return render(request, 'index.html', response)

def about(request):
	response = {}
	return render(request, 'about.html', response)

def katalog(request):
	response = {}
	response['apt'] = Apartment.objects.prefetch_related('room_set')
	return render(request, 'katalog.html', response)

def apartment(request,aptId):
	response= {}
	response['apt'] = Apartment.objects.prefetch_related('room_set').get(id=aptId)
	return render(request, 'apartment.html', response)

def room(request,roomId):
	response= {}
	response['room'] = Room.objects.get(id=roomId)
	response['room_lain'] = Room.objects.all()
	return render(request, 'room.html', response)


def blog(request):
	blog_list = Blog.objects.all()
	paginate = Paginator(blog_list, 5)
	page = request.GET.get('page')
	blogs = paginate.get_page(page)
	return render(request, 'blog.html', {'blogs': blogs})

def blog_detail(request, blogId):
	response = {}
	response['blog'] = Blog.objects.get(id = blogId)
	return render(request, 'blog_detail.html', response)