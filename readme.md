# Mahasewa Geprek

## Description

> Mahasewa Indonesia Website

## Forking Repository

1. Fork repo mhsw
2. Set repo yang sudah di fork menjadi remote origin

```git
git remote add origin https://gitlab.com/your_username/mhsw-geprek.git
```
3. Set repo mhsw-geprek jadi remote upstream

```git
git remote add upstream https://gitlab.com/mahasewa.id/mhsw-geprek.git
```


## Deployment

1. Setiap mengerjakan fitur baru, buat branch sesuai dengan nama fitur ditambah nama panggilan contoh : nata_login_form
2. Contoh untuk membuat branch adalah

```git
git checkout -b nama_brach_sesuai_format
# contoh git checkout -b nata_login_form
# init comment ya: untuk ganti branch sama, tapi tanpa flag -b
```

3. Setelah masuk ke branch, lakukan hal yang harus dilakukan (ngoding)
4. setelah bebetapa perubahan bisa add perubahan dan commit

```git
git add -A
git commit -m "kasih pesan yang berguna"
```

5. Push commit an anda pada branch anda sendiri dan pada origin bukan upstream (INGAT!: JANGAN PUSH KE MASTER)

```git
git push -u nama_branch_anda
# contoh : git push -u nata_login_form
```
6. Setelah push ke branch sendiri, buat lah PR(Pull Request)
7. PR nya ke repo mahasewa nya bukan diri sendiri
8. Jika sudah beres dan PR juga sudah beres jangan lupa pindahkan card anda ke on test
9. Jika sudah beres, maka pindahkan card ke Done
10. Jika masih ada revisi bisa ubah ulang ke branch yang sebelum nya dan coba benarkan revisinya
